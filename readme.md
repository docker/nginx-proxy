# Docker Nginx-Proxy Setup Guide

This guide outlines setting up a development environment using Docker and nginx-proxy, an automated nginx proxy for Docker containers. 

## Install Docker

Install Docker on your machine. Docker Desktop is available for Mac, Windows, and Linux. Download it from [Docker Desktop](https://www.docker.com/products/docker-desktop).

## Setting Up NGINX-Proxy

Use the terminal for these steps:

1. Clone the nginx-proxy repository: `git clone git@gitlab.msu.edu:ITServices-TLT-PS/nginx-proxy.git`. Place it in any directory; the default "nginx-proxy" folder works fine.
2. Inside the cloned repository, create a **certs** folder for SSL keys and certificates: `mkdir certs`.
3. Launch nginx-proxy using Docker Compose: Run `docker compose up -d`. This starts the proxy server, allowing app access similar to MAMP: navigate to `APP_NAME.localhost` in your browser.
4. Your nginx-proxy is now ready to host new sites.

## Adding a Host

1. Modify the environment variables in your site's `docker-compose.yml` for nginx-proxy compatibility. Example `docker-compose.yml`:

    ```yaml
    services:
      web:
        image: php7.4-apache:alpine
        environment:
          - VIRTUAL_HOST=example.localhost
          - VIRTUAL_PORT=443
          - VIRTUAL_PROTO=https
        volumes:
          - ../:/app
        expose:
          - 443
    ```

2. Generate SSL certificates for your domain (e.g., `example.localhost`) and save them in the **certs** folder. Use this format: `example.localhost.key` for the key and `example.localhost.crt` for the certificate. Use [mkcert](https://github.com/FiloSottile/mkcert) for certificate creation. Install mkcert, then generate certificates: `mkcert -key-file example.localhost.key -cert-file example.localhost.crt example.localhost`.
3. Since domains now end in `.localhost`, editing the hosts file is unnecessary.
4. Start the nginx-proxy Docker container with `docker compose up -d`.

## Networking Considerations

Ensure all Docker containers you wish to use with nginx-proxy are on the same network. Include this network configuration in your app's `docker-compose.yml`:

```yaml
networks:
  default:
    name: nginx-proxy
    external: true
```

Note: We no longer use Dockstation for container management. Docker Desktop is recommended for controlling nginx-proxy and other Docker containers.