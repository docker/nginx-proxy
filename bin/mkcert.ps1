# Check if mkcert is installed
if (-not (Get-Command mkcert -ErrorAction SilentlyContinue)) {
  Write-Host "mkcert is not installed. Please install mkcert (https://github.com/FiloSottile/mkcert) and try again."
  exit 1
}

# Create certs directory if it doesn't exist
if (-not (Test-Path "certs")) {
  New-Item -ItemType Directory -Path "certs" | Out-Null
}

# Ask for domain name if not provided
$domain = Read-Host "Enter domain name:"

# Generate SSL certificate for domain and wildcard subdomain
& mkcert -cert-file "certs/${domain}.crt" -key-file "certs/${domain}.key" "${domain}" "*.${domain}"
