#!/bin/bash

# Set the script's directory as the CWD
cd "$(dirname "$0")"

# Check if mkcert is installed
if ! command -v mkcert &>/dev/null; then
  echo "mkcert is not installed. Please install mkcert (https://github.com/FiloSottile/mkcert) and try again."
  exit 1
fi

# Create certs directory if it doesn't exist
if [ ! -d "../certs" ]; then
  mkdir certs
fi

# Ask for domain name if not provided
read -p "Enter domain name: " domain

# Generate SSL certificate for domain and wildcard subdomain
mkcert -cert-file "../certs/${domain}.crt" -key-file "../certs/${domain}.key" "${domain}" "*.${domain}"
